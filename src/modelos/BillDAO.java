package modelos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.util.PSQLException;

import beans.Bill;
import beans.User;

/**
 * Gestiona todo lo relacionado con las facturas.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class BillDAO {
	
	Conexion con = new Conexion();
	private final static String NORMAL_TABLE = "bills";
	private final static String PHANTOM_TABLE = "bills_phantom_payer";
	
	/**
	 * Añade las facturas generadas por un gasto en un evento.
	 * Si la factura ya existe para ese evento, actualiza la cantidad.
	 * @param idEvent es la id del evento.
	 * @param idReceiver es la id del usuario que recibira el pago.
	 * @param users es la lista de usuarios del evento.
	 * @param totalQuantity es la cantidad total del gasto.
	 * @throws SQLException
	 */
	
	public void addBills (int idEvent, int idReceiver, List<User> users, double totalQuantity) throws SQLException {
		int nUsers = users.size();
		PreparedStatement ps;
		ResultSet rs;
		String query = "";
		String table = "";		
		double quantity = totalQuantity / nUsers;
		for (User u : users) {
			if (u.isPhantom() || (!u.isPhantom() && u.getId()!=idReceiver)) {
				// cambio de tabla en funcion del tipo de usuario.
				if (u.isPhantom()) {
					table = PHANTOM_TABLE;				
				} else {
					table = NORMAL_TABLE;				
				}
				
				query = "SELECT * FROM "+table+" WHERE id_event=? AND id_payer=? AND id_receiver=?";
				ps = con.getConnection().prepareStatement(query);
				ps.setInt(1, idEvent);
				ps.setInt(2, u.getId());
				ps.setInt(3, idReceiver);
				rs = ps.executeQuery();
				if (rs.next()) {
					double oldQuantity = rs.getDouble(4);
					double quantityUpdate = quantity + oldQuantity;
					query = "UPDATE "+table+" SET quantity=? WHERE id_event=? AND id_payer=? AND id_receiver=?";
					ps = con.getConnection().prepareStatement(query);
					ps.setDouble(1, quantityUpdate);
					ps.setInt(2, idEvent);
					ps.setInt(3, u.getId());
					ps.setInt(4, idReceiver);
					ps.execute();
					ps.close();
				} else {
					query = "INSERT INTO "+table+" VALUES (?,?,?,?)";
					ps = con.getConnection().prepareStatement(query);
					ps.setInt(1, idEvent);
					ps.setInt(2, u.getId());
					ps.setInt(3, idReceiver);
					ps.setDouble(4, quantity);				
					ps.execute();
					ps.close();
				}

				// calculo de compensacion
				if (!u.isPhantom()) {					
					query = "SELECT quantity FROM bills WHERE id_event=? AND id_payer=? AND id_receiver=?";
					// miramos si existe la query inversa
					ps = con.getConnection().prepareStatement(query);
					ps.setInt(1, idEvent);
					ps.setInt(2, idReceiver);
					ps.setInt(3, u.getId());
					rs = ps.executeQuery();
					if (rs.next()) {
						double recDebt = rs.getDouble(1);
						query = "SELECT quantity FROM bills WHERE id_event=? AND id_payer=? AND id_receiver=?";
						ps = con.getConnection().prepareStatement(query);
						ps.setInt(1, idEvent);
						ps.setInt(2, u.getId());
						ps.setInt(3, idReceiver);
						rs = ps.executeQuery();
						rs.next();
						double payDebt = rs.getDouble(1);
						// comparamos deudas
						double total = payDebt - recDebt;
						if (total>0) {
							query = "UPDATE bills SET quantity=? WHERE id_event=? AND id_payer=? AND id_receiver=?";
							ps = con.getConnection().prepareStatement(query);
							ps.setDouble(1, total);
							ps.setInt(2, idEvent);
							ps.setInt(3, u.getId());
							ps.setInt(4, idReceiver);
							ps.execute();
							// borramos el inverso
							query = "DELETE FROM bills WHERE id_event=? AND id_payer=? AND id_receiver=?";
							ps = con.getConnection().prepareStatement(query);
							ps.setInt(1, idEvent);
							ps.setInt(2, idReceiver);
							ps.setInt(3, u.getId());
							ps.execute();
							ps.close();
						} if (total<0) {
							total = total*(-1);
							query = "UPDATE bills SET quantity=? WHERE id_event=? AND id_payer=? AND id_receiver=?";
							ps = con.getConnection().prepareStatement(query);
							ps.setDouble(1, total);
							ps.setInt(2, idEvent);
							ps.setInt(3, idReceiver);
							ps.setInt(4, u.getId());
							ps.execute();
							// borramos el inverso
							query = "DELETE FROM bills WHERE id_event=? AND id_payer=? AND id_receiver=?";
							ps = con.getConnection().prepareStatement(query);
							ps.setInt(1, idEvent);
							ps.setInt(2, u.getId());
							ps.setInt(3, idReceiver);
							ps.execute();
							ps.close();
						} else {
							query = "DELETE FROM bills WHERE id_event=? AND id_payer=? AND id_receiver=?";
							ps = con.getConnection().prepareStatement(query);
							ps.setInt(1, idEvent);
							ps.setInt(2, idReceiver);
							ps.setInt(3, u.getId());
							ps.execute();
							query = "DELETE FROM bills WHERE id_event=? AND id_payer=? AND id_receiver=?";
							ps = con.getConnection().prepareStatement(query);
							ps.setInt(1, idEvent);
							ps.setInt(2, u.getId());
							ps.setInt(3, idReceiver);
							ps.execute();
							ps.close();
						}
						
					}
					rs.close();
				}
				
				con.close();
			}
		}
	}
	
	/**
	 * Crea una lista con las facturas de cada evento segun el usuario.
	 * @param idEvent es la id del evento.
	 * @param idUser es la id del usuario.
	 * @return la lista de facturas.
	 */
	public List<Bill> listBills (int idEvent, int idUser) {
		List<Bill> res = new ArrayList<Bill>();
		PreparedStatement ps;
		String query = "";
		try {
			// usuarios normales
			query = "SELECT bills.*, (SELECT name FROM users WHERE id=id_payer) AS name_payer, "
					+ "(SELECT name FROM users WHERE id=id_receiver) AS name_receiver FROM bills "
					+ "WHERE id_event=? AND (id_payer=? OR id_receiver=?)";
			ps = con.getConnection().prepareStatement(query);
			ps.setInt(1, idEvent);
			ps.setInt(2, idUser);
			ps.setInt(3, idUser);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Bill b = new Bill (
						rs.getInt(1),
						rs.getInt(2),
						rs.getInt(3),
						rs.getDouble(4),
						rs.getString(5),
						rs.getString(6),
						false
						);
				res.add(b);
			}
			// usuarios fantasma
			query = "SELECT bills_phantom_payer.*, (SELECT name FROM users_phantom WHERE id=id_payer) AS name_payer, "
					+ "(SELECT name FROM users WHERE id=id_receiver) AS name_receiver FROM bills_phantom_payer "
					+ "WHERE id_event=? AND id_receiver=?";
			ps = con.getConnection().prepareStatement(query);
			ps.setInt(1, idEvent);
			ps.setInt(2, idUser);
			rs = ps.executeQuery();
			while (rs.next()) {
				Bill b = new Bill (
						rs.getInt(1),
						rs.getInt(2),
						rs.getInt(3),
						rs.getDouble(4),
						rs.getString(5),
						rs.getString(6),
						true
						);
				res.add(b);
			}
		} catch (PSQLException e1) {
			System.out.println("Fallo al listar las facturas.");
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return res;
	}

}
