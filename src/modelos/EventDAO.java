package modelos;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.util.PSQLException;

import beans.Event;
import beans.Expense;
import beans.User;

/**
 * Clase administradora de eventos.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class EventDAO {
	
	Conexion con = new Conexion();
	
	/**
	 * Devuelve los eventos relacionados con el usuario cliente.
	 * @param userId es la id del usuario cliente.
	 * @return una lista de eventos.
	 */
	public List<Event> userEvents (int userId) {
		List<Event> events = new ArrayList<Event>();
		String query = "SELECT events.*,confirmet FROM events,users_events WHERE id=id_event AND status!=3 AND confirmet!=2 AND id_user="+userId;
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Event e = new Event();
				e.setId(rs.getInt(1));
				e.setName(rs.getString(2));
				e.setDescription(rs.getString(3));
				e.setDateEvent(rs.getDate(4));
				e.setPlace(rs.getString(5));
				e.setIdAdmin(rs.getInt(6));
				e.setStatus(rs.getInt(8));
				e.setPhoto(rs.getBytes(9));
				e.setAmount(rs.getDouble(10)+"");
				e.setUserStatus(rs.getInt(11));
				events.add(e);
			}
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al recibir eventos del usuario.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return events;
	}
	
	/**
	 * Añade un nuevo evento a la base de datos
	 * @param e es el evento nuevo.
	 * @return el codigo de éxito o error.
	 */
	public Event addEvent (Event e) {
		Event event = null;
		String query = "INSERT INTO events (name, description, data_event, place, id_admin, import) VALUES (?,?,?,?,?,?)";
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, e.getName());
			ps.setString(2, e.getDescription());
			ps.setDate(3, e.getDateEvent());
			ps.setString(4, e.getPlace());
			ps.setInt(5, e.getIdAdmin());
			ps.setDouble(6, 0);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			int id_event = rs.getInt(1);
			if (Double.parseDouble(e.getAmount())!=0) {
				query = "INSERT INTO event_expense (id_event, id_user, import) VALUES (?,?,?)";
				ps = con.getConnection().prepareStatement(query);
				ps.setInt(1, id_event);
				ps.setInt(2, e.getIdAdmin());
				ps.setDouble(3, Double.parseDouble(e.getAmount()));
				ps.execute();
			}
			
			query = "SELECT * FROM events WHERE id="+id_event;
			ps = con.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			rs.next();
			event = new Event();
			event.setId(rs.getInt(1));
			event.setName(rs.getString(2));
			event.setDescription(rs.getString(3));
			event.setDateEvent(rs.getDate(4));
			event.setPlace(rs.getString(5));
			event.setIdAdmin(rs.getInt(6));
			event.setStatus(rs.getInt(8));
			event.setPhoto(rs.getBytes(9));
			event.setAmount(rs.getDouble(10)+"");
			
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e1) {
			System.out.println("Fallo al insertar el nuevo evento.");
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return event;
	}
	
	/**
	 * Lista los usuarios pertenecientes a un evento dado.
	 * @param eventid es la id del evento.
	 * @return la lista de usuarios del evento.
	 */
	public List<User> listUsers (int eventid) {
		List<User> res = new ArrayList<User>();
		// select * from users inner join users_events ON id=id_user Where id_event=1;
		String query = "SELECT id, name, mail, phone, photo_user, confirmet FROM users INNER JOIN users_events ON id=id_user WHERE id_event="+eventid;
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				User u = new User(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getBytes(5),
						rs.getInt(6)
						);
				res.add(u);
			}
			// usuarios fantasma, con confirmet 1
			query = "SELECT id, name, mail, phone FROM users_phantom INNER JOIN users_events_phantom ON id=id_user_phantom WHERE id_event="+eventid;
			ps = con.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()) {
				User u = new User(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						2, //confirmado
						true
						);
				res.add(u);
			}
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al recibir contactos");			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}	
	
	
	/**
	 * Actualiza la foto de evento dada una id.
	 * @param id es la id del evento.
	 * @param imgBytes es la imagen en array de bytes.
	 */
	public void updatePhoto(String id, byte[] imgBytes) {
		String query = "UPDATE events SET photo=? WHERE id=?";
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ps.setBytes(1, imgBytes);
			ps.setInt(2, Integer.parseInt(id));
			ps.execute();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al actualizar la foto de evento.");	
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Retorna una foto dada una id de evento.
	 * @param id es la id del evento.
	 * @return la foto en array de bytes.
	 */
	public byte[] getPhoto(String id) {
		byte[] img = null;
		String query = "SELECT photo FROM events WHERE id="+id;
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			rs.next();
			img = rs.getBytes(1);
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al recibir la foto de evento.");	
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return img;
	}
	
	/**
	 * Recibe una lista de ids de usuario y los añade al evento.
	 * @param ids son las id de los usuarios.
	 * @param eventId es la id del evento.
	 * @return el código de error/éxito.
	 */
	public int addUsers (List<String> ids, String eventId) {
		int code = 400;
		PreparedStatement ps;
		try {
			for (String id : ids) {	
				// control de usuario único insertado para el evento
				String query = "SELECT * FROM users_events WHERE id_event=? AND id_user=?";
				ps = con.getConnection().prepareStatement(query);
				ps.setInt(1, Integer.parseInt(eventId));
				ps.setInt(2, Integer.parseInt(id));
				ResultSet rs = ps.executeQuery();
				if (!rs.next()) {				
					query = "INSERT INTO users_events (id_event, id_user) VALUES ("+eventId+", "+id+")";
					ps = con.getConnection().prepareStatement(query);
					ps.execute();
				}
				rs.close();
				ps.close();
				con.close();
			}			
			code = 200;
		} catch (PSQLException e) {
			System.out.println("Fallo al insertar usuario al evento.");	
			e.printStackTrace();
			return code;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return code;
		}
		return code;
		
	}
	
	/**
	 * Cambia el estado de un usuario en un evento:
	 * confirma o rechaza su participación.
	 * 0 en espera de confirmación, 1 confirmado, 2 rechazado
	 * Se borrará el registro si el usuario rechaza la invitación.
	 * @param userId es la id de usuario.
	 * @param eventId es la id del evento.
	 * @param state es el estado.
	 * @return el código de éxito/error.
	 */
	public int changeUserState (String userId, String eventId, String state) {
		int code = 400;
		PreparedStatement ps;
		String query = "UPDATE users_events SET confirmet=? WHERE id_event=? AND id_user=?";
		try {
			if (state.equals("2")) {
				// evento rechazado
				query = "DELETE FROM users_events WHERE id_event=? AND id_user=?";
				ps = con.getConnection().prepareStatement(query);
				ps.setInt(1, Integer.parseInt(eventId));
				ps.setInt(2, Integer.parseInt(userId));
				ps.execute();
			} else {
				// cambio de estado
				query = "UPDATE users_events SET confirmet=? WHERE id_event=? AND id_user=?";
				ps = con.getConnection().prepareStatement(query);
				ps.setInt(1, Integer.parseInt(state));
				ps.setInt(2, Integer.parseInt(eventId));
				ps.setInt(3, Integer.parseInt(userId));
				ps.execute();				
			}
			code = 200;
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al cambiar el estado del usuario en el evento.");	
			e.printStackTrace();
			return code;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return code;
		}
		
		return code;
	}
	
	/**
	 * Actualiza el estado del evento:
	 * 1 añadiendo gastos, 2 realizando pagos, o 3 cerrado
	 * Si el estado cambia a 2, calculamos las facturas por cada gasto efectuado.
	 * @param eventId es la id del evento.
	 * @param state es el nuevo estado.
	 * @return el código de éxito/error.
	 */
	public int changeState (String eventId, String state) {
		int code = 400;
		int status = Integer.parseInt(state);
		int idEvent = Integer.parseInt(eventId);
		PreparedStatement ps;
		String query = "UPDATE events SET status=? WHERE id=?";
		try {
			ps = con.getConnection().prepareStatement(query);
			ps.setInt(1, status);
			ps.setInt(2, idEvent);
			ps.execute();
			// si el evento cambia al estado "realizar pagos", calculamos las facturas
			if (status==2) {
				// eliminamos los usuarios que no confirmaron
				query = "DELETE FROM users_events WHERE confirmet=0 AND id_event="+idEvent;
				ps = con.getConnection().prepareStatement(query);
				ps.execute();
				// recuperamos la lista de gastos
				ExpenseDAO edao = new ExpenseDAO();
				List<Expense> exps = edao.listExpenses(idEvent);
				// recuperamos los participantes para la reparticion
				List<User> users = listUsers(idEvent);
				for (Expense e : exps) {
					int idReceiver = e.getId_user();
					double totalQuantity = e.getQuantity();
					BillDAO bdao = new BillDAO();
					// calculamos y añadimos a la tabla bills (o bills_phantom_payer)
					bdao.addBills(idEvent, idReceiver, users, totalQuantity);
				}
			}
			code = 200;
			ps.close();
			con.close();			
		} catch (PSQLException e) {
			System.out.println("Fallo al cambiar el estado del evento.");	
			e.printStackTrace();
			return code;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return code;
		}
		
		return code;
	}
	
	/**
	 * Añade un usuario fantasma a un evento
	 * @param eventId es la id del evento.
	 * @param name es el nombre del usuario fantasma.
	 * @return el código de éxito/error.
	 */
	public int addPhantom (String eventId, String name) {
		int code = 400;
		PreparedStatement ps;
		String query = "INSERT INTO users_phantom (name) VALUES ('"+name+"')";		
		try {
			ps = con.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			int id_phantom = rs.getInt(1);
			query = "INSERT INTO users_events_phantom (id_event, id_user_phantom) VALUES (?,?)";
			ps = con.getConnection().prepareStatement(query);
			ps.setInt(1, Integer.parseInt(eventId));
			ps.setInt(2, id_phantom);
			ps.execute();
			code = 200;
			ps.close();
			rs.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al insertar usuario fantasma en el evento.");	
			e.printStackTrace();
			return code;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return code;
		}		
		return code;
	}

}
