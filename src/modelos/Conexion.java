package modelos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Clase que conecta con la base de datos del servidor remoto.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Conexion {

	Connection connection = null;
	String host = System.getenv("OPENSHIFT_POSTGRESQL_DB_HOST");
	String port = System.getenv("OPENSHIFT_POSTGRESQL_DB_PORT");
	private String url = "jdbc:postgresql://"+host+":"+port+"/servlet";
	private String user = "adminw7cbfpi";
	private String pass = "Da9BtU82NT67";
	
	// host: 127.7.18.130
	// port: 5432
	
	// PSQL SERVER
	// psql -U adminw7cbfpi servlet
	
	// codificacion
	// update pg_database set encoding=8 where datname='servlet';
	
	// IMPORTAR ARCHIVOS
	// /var/lib/openshift/55365cfee0b8cd987f00004f/app-deployments/current/repo/tmp/
	
	// RESET
	// drop schema public cascade;
	// create schema public;
	
	public Conexion() {}
	
	
	public Connection getConnection() {
		loadDriver();
		connect();
		return connection;
	}
	
	public void loadDriver() {		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void connect() {
		try {
			connection = DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
