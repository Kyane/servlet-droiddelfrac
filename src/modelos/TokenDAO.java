package modelos;

import java.io.PrintWriter;

import org.json.JSONException;
import org.json.JSONObject;

import exceptions.TokenException;

/**
 * Clase administradora de los tokens necesarios para acceder al servidor.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class TokenDAO {
	
	/**
	 * Devuelve el token.
	 * @return el token.
	 */
	public static String getToken() {
		return "12345678";
	}
	
	/**
	 * Revisa que el token recibido sea válido.
	 * @param token es el token recibido.
	 * @throws TokenException
	 */
	public static void validToken(String token) throws TokenException {
		if (!token.equals(getToken())) {
			throw new TokenException("El token no coincide.");
		}
	}
	
	/**
	 * Crea un JSON genérico para el TokenException.
	 */
	public static JSONObject tokenExceptionJson(PrintWriter out) {
		JSONObject res = new JSONObject();
		try {
			res.put("code", 498);
			JSONObject data = new JSONObject();
			data.put("message", "ERROR");
			data.put("type", "Token expired/invalid");
			res.put("error", data);
			out.print(res.toString(4));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return res;
	}
}
