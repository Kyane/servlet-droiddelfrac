package modelos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.util.PSQLException;

import beans.Expense;

/**
 * Gestiona los gastos de los eventos guardados en la base de datos.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class ExpenseDAO {
	
	Conexion con = new Conexion();
	
	/**
	 * Añade un nuevo gasto a un evento existente y genera las facturas para cada usuario pagador.
	 * @param e es el gasto.
	 * @return el código de éxito/error.
	 */
	public int addExpense (Expense e) {
		int code = 400;
		String query = "INSERT INTO event_expense (id_event, id_user, concept, import) VALUES (?,?,?,?)";
		PreparedStatement ps;
		int idEvent = e.getId_event();
		int idReceiver = e.getId_user();
		double totalQuantity = e.getQuantity();
		try {
			// inserción del gasto
			ps = con.getConnection().prepareStatement(query);
			ps.setInt(1, idEvent);
			ps.setInt(2, idReceiver);
			ps.setString(3, e.getConcept());
			ps.setDouble(4, totalQuantity);
			ps.execute();						
			code = 200;
			ps.close();
			con.close();
		} catch (PSQLException e1) {
			System.out.println("Fallo al insertar el nuevo gasto.");
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println("SQL: Fallo al insertar el nuevo gasto.");
			e1.printStackTrace();
		}
		return code;
		
	}
	
	/**
	 * Lista los gastos para un evento.
	 * @param eventid es la id del evento.
	 * @return la lista de gastos.
	 */
	public List<Expense> listExpenses(int eventid) {
		List<Expense> res = new ArrayList<Expense>();
		String query = "SELECT event_expense.*, name FROM event_expense INNER JOIN users ON users.id=id_user WHERE id_event="+eventid;
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Expense e = new Expense (
						rs.getInt(1),
						rs.getInt(2),
						rs.getInt(3),
						rs.getString(4),
						rs.getDouble(5),
						rs.getString(6)
						);
				res.add(e);
			}
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e1) {
			System.out.println("Fallo al listar los gastos.");
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return res;
	}

}
