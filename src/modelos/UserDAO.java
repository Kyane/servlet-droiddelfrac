package modelos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.util.PSQLException;

import beans.User;

/**
 * Clase que administra los usuarios en base de datos.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class UserDAO {
	
	Conexion con = new Conexion();
	
	/**
	 * Verifica que existe un usuario en la base de datos donde coincida
	 * el email y la contraseña.
	 * @param email es el email del usuario.
	 * @param pass es la contraseña del usuario.
	 * @return el usuario conectado.
	 */
	public User verifyUser(User u) {
		User res = null;
		String query = "SELECT validation_user('"+u.getEmail()+"', '"+u.getPass()+"')";
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			rs.next();
			int id = rs.getInt(1);
			query = "SELECT name, mail, photo_user FROM users WHERE id="+id;			
			ps = con.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			rs.next();
			res = new User (
					id,
					rs.getString(1),
					rs.getString(2),
					rs.getBytes(3)
					);
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al validar usuario");	
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	
	/**
	 * Inserta usuarios nuevos en la base de datos.
	 * @param u es el nuevo usuario.
	 * @return si ha sido añadido con un código de error/éxito.
	 */
	public int insertUser (User u) {
		String query = "";
		int res = -1;
		// control de variables null
		String phone = "null";
		if (!u.getPhone().equals("")) {
			phone = "'"+u.getPhone()+"'";
		}
		query = "SELECT * FROM insert_user('', '"+u.getEmail()+"', "+phone+", '"+u.getPass()+"')";

		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			rs.next();
			res = rs.getInt(1);
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			// captura de la constraint violada
			System.out.println("Fallo al insertar el user");
			e.printStackTrace();
			if (e.getMessage().indexOf("mail_unique")!=-1) {
				System.out.println("Mail unique");
				res=-3;
			}
			if (e.getMessage().indexOf("phone_unique")!=-1) {
				System.out.println("Phone unique");
				res=-4;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	
	/**
	 * Lista los contactos del usuario cliente.
	 * @param userid es la id del usuario cliente.
	 * @return la lista de usuarios.
	 */
	public List<User> listFriends (int userid) {
		List <User> res = new ArrayList<User>();
		// select * from users inner join (select get_contacts(1)) as amigos ON id=get_contacts;
		String query = "SELECT id, name, mail, phone, photo_user FROM users INNER JOIN (SELECT get_contacts("+userid+")) AS friends ON id=get_contacts";
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				User u = new User(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getBytes(5)
						);
				res.add(u);
			}			
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al recibir contactos");			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	
	/**
	 * Actualiza un usuario existente con datos extra.
	 * @param u es el usuario a actualizar.
	 * @return el código de éxito/error.
	 */
	public int updateName(User u) {
		int code = 400;
		int id = u.getId();
		String name = u.getName();
		String query = "";
		PreparedStatement ps;
		// control de nombre vacío, añadimos el email
		if (name.equals("")) {
			query = "SELECT mail FROM users WHERE id="+id;
			try {
				ps = con.getConnection().prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				rs.next();
				name = rs.getString(1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		
		query = "UPDATE users SET name='"+name+"' WHERE id="+id;		
		try {
			ps = con.getConnection().prepareStatement(query);
			ps.execute();
			code = 200;
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al actualizar el usuario.");	
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;
	}
	
	/**
	 * Actualiza la foto de usuario dada una id.
	 * @param id es la id del usuario.
	 * @param imgBytes es la imagen en array de bytes.
	 */
	public void updatePhoto(String id, byte[] imgBytes) {
		String query = "UPDATE users SET photo_user=? WHERE id=?";
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ps.setBytes(1, imgBytes);
			ps.setInt(2, Integer.parseInt(id));
			ps.execute();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al actualizar la foto de usuario.");	
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Retorna una foto dada una id de usuario.
	 * @param id es la id del usuario.
	 * @return la foto en array de bytes.
	 */
	public byte[] getPhoto(String id) {
		byte[] img = null;
		String query = "SELECT photo FROM users WHERE id="+id;
		PreparedStatement ps;
		try {
			ps = con.getConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			rs.next();
			img = rs.getBytes(1);
			System.out.println("FOTO: "+img);
			rs.close();
			ps.close();
			con.close();
		} catch (PSQLException e) {
			System.out.println("Fallo al recibir la foto de usuario.");	
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return img;
	}
	
	/**
	 * Dada una lista de teléfonos, retorna los que ya son usuarios
	 * de la aplicación.
	 * @param nums son los números de teléfono a comparar.
	 * @return los que ya son usuarios.
	 */
	public List<User> areUsers (List<String> nums) {
		List<User> res = new ArrayList<User>();
		PreparedStatement ps;
		ResultSet rs;
		try {
			for (String n : nums) {
				String query = "SELECT id, name, mail, phone, photo_user FROM users WHERE phone='"+n+"'";
			
				ps = con.getConnection().prepareStatement(query);
				rs = ps.executeQuery();
				if(rs.next()) {
					User u = new User (
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getBytes(5)
							);	
					res.add(u);
				}
				rs.close();
				ps.close();
			}
			con.close();
			
		} catch (PSQLException e) {
			System.out.println("Fallo al recibir la foto de usuario.");	
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}

}
