package beans;

import java.sql.Date;

/**
 * Modela un evento para la conexión con la base de datos.
 /**
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Event {
	
	private int id;
	private String name;
	private String description;
	/*
	 * status 1 = evento creado. Se pueden agregar gastos
	 * status 2 = evento pendiente de pago. No se pueden notificar mas gastos
	 * status 3 = evento cerrado. Todos los cobros realizados
	 */
	private int status;
	/*
	 * userStatus 0 = en espera de confirmación
	 * userStatus 1 = confirmado
	 * userStatus 2 = rechazado
	 */
	private int userStatus;
	private String amount;
	private Date dateEvent;
	private String place;
	private int idAdmin;
	private byte[] photo;
	
	public Event() {
		
	}
	
	/**
	 * Constructor para CreateEvent, la creación de un nuevo evento.
	 * @param name es el nombre del evento.
	 * @param description es la descripción.
	 * @param data_event es la fecha de celebración.
	 * @param place es el lugar de celebración.
	 * @param id_admin es la id del usuario administrador.
	 */
	public Event(String name, String description, String place, int idAdmin) {
		this.name = name;
		this.description = description;
		this.place = place;
		this.idAdmin = idAdmin;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Date getDateEvent() {
		return dateEvent;
	}

	public void setDateEvent(Date dateEvent) {
		this.dateEvent = dateEvent;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public int getIdAdmin() {
		return idAdmin;
	}

	public void setIdAdmin(int idAdmin) {
		this.idAdmin = idAdmin;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}	
	
}
