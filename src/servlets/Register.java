package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import exceptions.TokenException;
import modelos.TokenDAO;
import modelos.UserDAO;
import beans.User;

/**
 * Servlet de registro de usuarios en la base de datos.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject res = new JSONObject();
		
		try {
			TokenDAO.validToken(request.getParameterValues("token")[0]);		
		
			User u = new User( 
					request.getParameterValues("email")[0], 
					request.getParameterValues("phone")[0],
					request.getParameterValues("pass")[0]);		
			UserDAO dao = new UserDAO();
			int id = dao.insertUser(u);			

			// si devuelve -1 o menor, el usuario no se ha insertado.
			if (id < 0) {
				res.put("code", 400);
				JSONObject data = new JSONObject();
				data.put("message", "ERROR");
				// captura del tipo de constraint violada
				if (id==-3) {
					data.put("type", "mail_unique");
				} else if (id==-4) {
					data.put("type", "phone_unique");
				} else {
					data.put("type", "Bad request");
				}
				res.put("error", data);
			} else {
				res.put("code", 200);
				JSONObject data = new JSONObject();
				data.put("id_user", id);
				data.put("token", TokenDAO.getToken());
				res.put("data", data);				
			}
			out.print(res.toString(4));
		} catch (TokenException e1) {
			res=TokenDAO.tokenExceptionJson(out);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
