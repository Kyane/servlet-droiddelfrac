package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.EventDAO;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet que cambia el estado de un evento para un usuario,
 * es decir, si confirma o rechaza su participación.
 * 0 en espera de confirmación, 1 confirmado, 2 rechazado.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/usereventstate") //&userid &eventid &state
public class UserEventState extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEventState() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject res = new JSONObject();
		
		String userId = request.getParameterValues("userid")[0];
		String eventId = request.getParameterValues("eventid")[0];
		String state = request.getParameterValues("state")[0];
		
		EventDAO dao = new EventDAO();
		int code = dao.changeUserState(userId, eventId, state);
		try {
			if (code!=200) {		
				res.put("code", 400);
				JSONObject data = new JSONObject();
				data.put("message", "ERROR");
				data.put("type", "Bad request");
				res.put("error", data);
			} else {
				res.put("code", 200);
				JSONObject data = new JSONObject();
				data.put("message", "OK");
				res.put("data", data);
			}
			out.print(res.toString(4));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
