package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.UserDAO;

import org.json.JSONException;
import org.json.JSONObject;

import beans.ListStrings;
import beans.ListUsers;
import beans.User;

import com.google.gson.Gson;

/**
 * Servlet que recibe una lista de telefonos y los compara con los de la base de datos.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/existingusers")
public class ExistingUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExistingUsers() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		JSONObject res = new JSONObject();
		String result = null;
		String json = request.getHeader("lista");
		try {
			JSONObject jObj = new JSONObject(json);
			result = String.valueOf(jObj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ListStrings numslist = new Gson().fromJson(result, ListStrings.class);
		List<String> nums = numslist.getListStrings();
		
		UserDAO dao = new UserDAO();
		List<User> users = dao.areUsers(nums);
		JSONObject data = new JSONObject();
		try {
			ListUsers lu = new ListUsers(users);
			String user = new Gson().toJson(lu);
			data.put("user", user);
			res.put("code", 200);
			res.put("data", data);
			out.print(res.toString(4));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
