package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelos.EventDAO;
import org.json.JSONException;
import org.json.JSONObject;
import beans.ListStrings;

import com.google.gson.Gson;

/**
 * Servlet que recibe una lista de id de usuario y los añade al evento
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/addusers")
public class AddUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUsers() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		JSONObject res = new JSONObject();
		String result = null;
		String json = request.getHeader("lista");
		try {
			JSONObject jObj = new JSONObject(json);
			result = String.valueOf(jObj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ListStrings idlist = new Gson().fromJson(result, ListStrings.class);
		List<String> ids = idlist.getListStrings();
		
		String eventId = request.getParameterValues("eventid")[0];
		EventDAO dao = new EventDAO();
		int code = dao.addUsers(ids, eventId);
		JSONObject data = new JSONObject();
		try {
			if (code==200) {
				res.put("code", code);				
				data.put("message", "OK");
				res.put("data", data);
			} else {
				res.put("code", 400);
				data.put("message", "ERROR");
				data.put("type", "Bad request");
				res.put("error", data);
			}
			out.print(res.toString(4));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
