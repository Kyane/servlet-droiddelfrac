package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.TokenDAO;
import modelos.UserDAO;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import beans.ListUsers;
import beans.User;
import exceptions.TokenException;

/**
 * Servlet de autentificación del usuario en la pantalla de login.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject res = new JSONObject();
		
		try {
			TokenDAO.validToken(request.getParameterValues("token")[0]);

			User u = new User(request.getParameterValues("email")[0], request.getParameterValues("pass")[0]);
			UserDAO dao = new UserDAO();
			
			User logged = dao.verifyUser(u);	
			List<User>list = new ArrayList<User>();
			list.add(logged);
			

			// si devuelve -1, el usuario y el password no coinciden (error).
			if (logged == null) {
				res.put("code", 401);
				JSONObject data = new JSONObject();
				data.put("message", "ERROR");
				data.put("type", "Unauthorized");
				res.put("error", data);
			} else {
				ListUsers le = new ListUsers(list);
				String user = new Gson().toJson(le);
				res.put("code", 200);
				JSONObject data = new JSONObject();
				data.put("user", user);
				data.put("token", TokenDAO.getToken());
				res.put("data", data);
				
			}
			out.print(res.toString(4));
		} catch (TokenException e1) {
			res=TokenDAO.tokenExceptionJson(out);			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
