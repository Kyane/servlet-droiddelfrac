package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.ExpenseDAO;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import beans.Expense;
import beans.ListEvExpenses;

/**
 * Servlet que lista los gastos de un evento.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/listexpenses")
public class ListExpenses extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListExpenses() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject res = new JSONObject();
		
		int eventId = Integer.parseInt(request.getParameterValues("eventid")[0]);
		ExpenseDAO dao = new ExpenseDAO();
		List<Expense> expenses = dao.listExpenses(eventId);
		
		JSONObject data = new JSONObject();
		try {
			ListEvExpenses le = new ListEvExpenses(expenses);
			String expense = new Gson().toJson(le);
			data.put("expense", expense);
			res.put("code", 200);
			res.put("data", data);
			out.print(res.toString(4));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
