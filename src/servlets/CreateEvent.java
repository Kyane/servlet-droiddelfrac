package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelos.EventDAO;
import org.json.JSONException;
import org.json.JSONObject;

import beans.Event;

/**
 * Servlet encargado de manejar los nuevos eventos creados por el usuario cliente.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/createevent")
public class CreateEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateEvent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject res = new JSONObject();
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			Event e = new Event(
					request.getParameterValues("name")[0],
					request.getParameterValues("description")[0],
					request.getParameterValues("place")[0],
					Integer.parseInt(request.getParameterValues("idadmin")[0])
					);
			if (request.getParameterValues("import")[0]!="") {
				e.setAmount(request.getParameterValues("import")[0]);
			} else {
				e.setAmount("0");
			}
			if (request.getParameterValues("dataevent")[0]!="") {
				java.util.Date utilDate = df.parse(request.getParameterValues("dataevent")[0]);
				java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
				e.setDateEvent(sqlDate);
			}
			EventDAO dao = new EventDAO();
			Event event = dao.addEvent(e);
			JSONObject data = new JSONObject();
			if (event!=null) {
				res.put("code", 200);				
				data.put("id", event.getId());
				data.put("name", event.getName());
				data.put("description", event.getDescription());
				data.put("data_event", event.getDateEvent());
				data.put("place", event.getPlace());
				data.put("id_admin", event.getIdAdmin());
				data.put("status", event.getStatus());
				data.put("photo", event.getPhoto());			
				data.put("import", event.getAmount());
				data.put("user_status", event.getUserStatus());
				res.put("data", data);
			} else {
				res.put("code", 400);
				data.put("message", "ERROR");
				data.put("type", "Bad request");
				res.put("error", data);
			}
			out.print(res.toString(4));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
