package servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.EventDAO;
import modelos.UserDAO;

/**
 * Servlet que controla las cargas y descargas de imagenes.
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/uploadimg")
@MultipartConfig
public class UploadImg extends HttpServlet {
	private static final long serialVersionUID = 1L;
	int BUFFER_LENGTH = 4096;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadImg() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameterValues("id")[0];
		UserDAO dao = new UserDAO();
		byte[] imgBytes = dao.getPhoto(id);

		 BufferedInputStream input = null;
	        BufferedOutputStream output = null;
	        response.setContentType("image/jpeg"); 
            response.setContentLength(imgBytes.length);
	        try {
	            input = new BufferedInputStream(new ByteArrayInputStream(imgBytes));
	            output = new BufferedOutputStream(response.getOutputStream());

	            byte[] buffer = new byte[10240];
	            int length = 0;
	            while ((length = input.read(buffer)) > 0) {
	                output.write(buffer, 0, length);
	                output.flush();
	                response.flushBuffer();
	            }
	        } finally {
	            input.close();
	            output.close();
	        }	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameterValues("id")[0];
		String type = request.getParameterValues("type")[0];
		InputStream is = request.getPart("userfile").getInputStream();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] bytes = new byte[BUFFER_LENGTH];
        int read = 0;
        while ((read = is.read(bytes, 0, BUFFER_LENGTH)) != -1) {
        	bos.write(bytes, 0, read);
        }
        bos.flush();
        byte[] imgBytes = bos.toByteArray();
        if (type.equals("user")) {
	        UserDAO dao = new UserDAO();	
	        dao.updatePhoto(id, imgBytes);
        } else if (type.equals("event")) {
        	EventDAO dao = new EventDAO();
        	dao.updatePhoto(id, imgBytes);
        }        
        is.close();
        bos.close();
	}

}
