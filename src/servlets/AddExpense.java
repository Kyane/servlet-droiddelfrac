package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.ExpenseDAO;

import org.json.JSONException;
import org.json.JSONObject;

import beans.Expense;

/**
 * Servlet encargado de añadir nuevos gastos a un evento
 * @author David Llorca Baron <dllorca.baron@gmail.com>
 * @author Jesica Perea Gil <jesspegil@gmail.com>
 * @author Antonio Riquelme Huerta <antonioriquelmeh@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
@WebServlet("/addexpense")
public class AddExpense extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddExpense() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject res = new JSONObject();
		try {
			Expense e = new Expense (
					Integer.parseInt(request.getParameterValues("id_event")[0]),
					Integer.parseInt(request.getParameterValues("id_user")[0]),
					request.getParameterValues("concept")[0],
					Double.parseDouble(request.getParameterValues("quantity")[0])
					);
			ExpenseDAO dao = new ExpenseDAO();
			int code = dao.addExpense(e);
			JSONObject data = new JSONObject();
			if (code==200) {
				res.put("code", code);				
				data.put("message", "OK");
				res.put("data", data);
			} else {
				res.put("code", 400);
				data.put("message", "ERROR");
				data.put("type", "Bad request");
				res.put("error", data);
			}
			out.print(res.toString(4));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
